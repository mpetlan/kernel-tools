#!/bin/sh
# Generate a commit for a rawhide RC release

klist -s
if [ ! $? -eq 0 ]; then
	echo "klist couldn't read the credential cache."
	echo "Do you need to fix your kerberos tokens?"
	exit 1
fi

# Figure out what is our RC
RC=`grep "%global rcrev" kernel-tools.spec| cut -d ' ' -f 3`
RC=$(($RC+1))
BASE=`grep "%global base_sublevel" kernel-tools.spec| cut -d ' ' -f 3`
OLDBASE=$BASE
# See comment in kernel-tools.spec about the base numbering
BASE=$(($BASE+1))
MAJORVER=5

# Kill all patches
awk '!/patch/ { print $0 }' < sources > sources.tmp
mv sources.tmp sources

# Grab the tarball
if [ ! -f patch-$MAJORVER.$BASE-rc$RC.xz ]; then
	wget -O patch-$MAJORVER.$BASE-rc$RC https://git.kernel.org/torvalds/p/v$MAJORVER.$BASE-rc$RC/v$MAJORVER.$OLDBASE
	if [ ! $? -eq 0 ]; then
		exit 1
	fi
	xz -9 patch-$MAJORVER.$BASE-rc$RC
	fedpkg upload patch-$MAJORVER.$BASE-rc$RC.xz
fi

# bump rcrev in the spec and set git snapshot to 0
RC=$RC perl -p -i -e 's|%global rcrev.*|%global rcrev $ENV{'RC'}|' kernel-tools.spec

perl -p -i -e 's|%define gitrev.*|%define gitrev 0|' kernel-tools.spec

perl -p -i -e 's|%global baserelease.*|%global baserelease 0|' kernel-tools.spec

rpmdev-bumpspec -c "Linux v$MAJORVER.$BASE-rc$RC" kernel-tools.spec
